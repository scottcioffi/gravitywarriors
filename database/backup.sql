-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: May 13, 2016 at 06:12 PM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `gw`
--

-- --------------------------------------------------------

--
-- Table structure for table `activitylog`
--

CREATE TABLE `activitylog` (
  `id` int(11) NOT NULL,
  `date` datetime NOT NULL,
  `username` varchar(30) NOT NULL,
  `action` varchar(100) NOT NULL,
  `additionalinfo` varchar(500) NOT NULL DEFAULT 'none',
  `ip` varchar(15) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `activitylog`
--

INSERT INTO `activitylog` (`id`, `date`, `username`, `action`, `additionalinfo`, `ip`) VALUES
(11, '2016-05-13 18:10:11', 'elena', 'AUTH_REGISTER_SUCCESS', 'Account created and activation email sent', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `attempts`
--

CREATE TABLE `attempts` (
  `ip` varchar(15) NOT NULL,
  `count` int(11) NOT NULL,
  `expiredate` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `exercises`
--

CREATE TABLE `exercises` (
  `id` smallint(6) NOT NULL,
  `name` text NOT NULL,
  `target` tinyint(4) NOT NULL,
  `primarygroup` enum('Abdominals','Biceps','Shoulders','Low Back','Calf','Glutes','Hamstrings','Back','Obliques','Chest','Quads','Traps','Triceps') NOT NULL,
  `volume` tinyint(4) NOT NULL,
  `startingrank` tinyint(4) NOT NULL,
  `favorite` tinyint(4) NOT NULL DEFAULT '3',
  `ban` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `exercises`
--

INSERT INTO `exercises` (`id`, `name`, `target`, `primarygroup`, `volume`, `startingrank`, `favorite`, `ban`) VALUES
(1, 'Pushups', 1, 'Chest', 3, 2, 3, 0),
(2, 'Pullup', 2, 'Back', 2, 3, 3, 0),
(3, 'Dragonfly', 3, 'Abdominals', 1, 4, 3, 0),
(4, 'Ring dips', 1, 'Triceps', 2, 4, 3, 0),
(5, 'Ring dip hold', 1, 'Triceps', 3, 3, 3, 0),
(6, 'Atg squats', 4, 'Quads', 3, 3, 3, 0),
(7, 'Pistol squats', 4, 'Quads', 1, 4, 3, 0),
(8, 'Chair hold', 4, 'Quads', 3, 1, 3, 0),
(9, 'Calf raises', 4, 'Calf', 3, 1, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `sessions`
--

CREATE TABLE `sessions` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `hash` varchar(32) NOT NULL,
  `expiredate` datetime NOT NULL,
  `ip` varchar(15) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sets`
--

CREATE TABLE `sets` (
  `id` int(11) NOT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `uid` int(11) NOT NULL,
  `exerciseid` int(11) NOT NULL,
  `workoutid` int(11) NOT NULL,
  `position` tinyint(4) NOT NULL,
  `totalreps` smallint(6) NOT NULL,
  `1` smallint(6) NOT NULL,
  `2` smallint(6) NOT NULL,
  `3` smallint(6) NOT NULL,
  `4` smallint(6) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sets`
--

INSERT INTO `sets` (`id`, `timestamp`, `uid`, `exerciseid`, `workoutid`, `position`, `totalreps`, `1`, `2`, `3`, `4`) VALUES
(2, '0000-00-00 00:00:00', 0, 8, 1, 2, 2000, 24, 240, 20, 2000),
(3, '0000-00-00 00:00:00', 0, 1, 1, 1, 0, 0, 0, 0, 0),
(4, '0000-00-00 00:00:00', 0, 1, 1, 1, 200, 34, 0, 0, 113),
(5, '0000-00-00 00:00:00', 0, 2, 1, 2, 300, 35, 0, 0, 0),
(6, '0000-00-00 00:00:00', 0, 3, 1, 3, 400, 3, 0, 0, 0),
(7, '0000-00-00 00:00:00', 0, 7, 1, 4, 500, 3, 0, 0, 0),
(8, '0000-00-00 00:00:00', 0, 9, 1, 5, 600, 2, 0, 0, 0),
(9, '2016-04-20 00:04:49', 0, 1, 1, 1, 200, 34, 0, 0, 113),
(10, '2016-04-20 00:04:49', 0, 2, 1, 2, 300, 35, 0, 0, 0),
(11, '2016-04-20 00:04:49', 0, 3, 1, 3, 400, 3, 0, 0, 0),
(12, '2016-04-20 00:04:49', 0, 7, 1, 4, 500, 3, 0, 0, 0),
(13, '2016-04-20 00:04:49', 0, 9, 1, 5, 600, 2, 0, 0, 0),
(14, '2016-04-20 00:11:05', 0, 2, 1, 1, 100, 48, 20, 0, 0),
(15, '2016-04-20 00:11:05', 0, 3, 1, 2, 100, 30, 0, 0, 0),
(16, '2016-04-20 00:11:17', 0, 2, 1, 1, 100, 48, 20, 0, 0),
(17, '2016-04-20 00:11:17', 0, 3, 1, 2, 100, 30, 0, 0, 0),
(18, '2016-04-20 00:13:36', 0, 1, 1, 1, 0, 0, 0, 0, 0),
(19, '2016-04-20 00:13:59', 0, 3, 1, 1, 0, 0, 0, 0, 0),
(20, '2016-04-20 00:14:06', 0, 3, 1, 1, 0, 0, 0, 0, 0),
(21, '2016-04-20 00:14:11', 0, 1, 1, 1, 0, 0, 0, 0, 0),
(22, '2016-04-20 00:16:04', 0, 2, 1, 1, 0, 0, 0, 0, 0),
(23, '2016-04-20 00:16:19', 0, 2, 1, 1, 0, 0, 0, 0, 0),
(24, '2016-04-20 00:16:38', 0, 1, 1, 1, 0, 0, 0, 0, 0),
(25, '2016-04-20 00:16:48', 0, 1, 1, 1, 0, 0, 0, 0, 0),
(26, '2016-04-20 00:41:34', 0, 1, 1, 1, 0, 0, 0, 0, 0),
(27, '2016-05-13 07:00:22', 0, 2, 1, 1, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(128) NOT NULL,
  `email` varchar(100) NOT NULL,
  `isactive` tinyint(1) NOT NULL DEFAULT '0',
  `activekey` varchar(15) NOT NULL DEFAULT '0',
  `resetkey` varchar(15) NOT NULL DEFAULT '0'
) ENGINE=MyISAM AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `isactive`, `activekey`, `resetkey`) VALUES
(73, 'elena', '6cebc34d3c259ca772cee66943016e8c51c646a657f17ff8b843e97d38e8b429fa285027c105b74298b9e674ab6b089c36d3074e1a1cce508f88ee7b45dd532a', 'elenagmz21@gmail.com', 0, 'bjAkkYLiVInf9zD', '0');

-- --------------------------------------------------------

--
-- Table structure for table `workouts`
--

CREATE TABLE `workouts` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `name` tinytext NOT NULL,
  `lastupdated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `workouts`
--

INSERT INTO `workouts` (`id`, `uid`, `name`, `lastupdated`) VALUES
(1, 2, '1', '0000-00-00 00:00:00'),
(2, 3, '1', '0000-00-00 00:00:00'),
(3, 5, '1', '0000-00-00 00:00:00'),
(4, 9, '1', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `activitylog`
--
ALTER TABLE `activitylog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `exercises`
--
ALTER TABLE `exercises`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sets`
--
ALTER TABLE `sets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `workouts`
--
ALTER TABLE `workouts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `activitylog`
--
ALTER TABLE `activitylog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `exercises`
--
ALTER TABLE `exercises`
  MODIFY `id` smallint(6) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `sets`
--
ALTER TABLE `sets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=74;
--
-- AUTO_INCREMENT for table `workouts`
--
ALTER TABLE `workouts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;