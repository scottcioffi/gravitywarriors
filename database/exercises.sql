-- phpMyAdmin SQL Dump
-- version 4.1.6
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jul 17, 2014 at 03:56 PM
-- Server version: 5.5.34
-- PHP Version: 5.4.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `gw`
--

-- --------------------------------------------------------

--
-- Table structure for table `exercises`
--

CREATE TABLE IF NOT EXISTS `exercises` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `target` tinyint(4) NOT NULL,
  `primarygroup` enum('Abdominals','Biceps','Shoulders','Low Back','Calf','Glutes','Hamstrings','Back','Obliques','Chest','Quads','Traps','Triceps') NOT NULL,
  `volume` tinyint(4) NOT NULL,
  `startingrank` tinyint(4) NOT NULL,
  `favorite` tinyint(4) NOT NULL DEFAULT '3',
  `ban` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `exercises`
--

INSERT INTO `exercises` (`id`, `name`, `target`, `primarygroup`, `volume`, `startingrank`, `favorite`, `ban`) VALUES
(1, 'Pushups', 1, 'Chest', 3, 2, 3, 0),
(2, 'Pullup', 2, 'Back', 2, 3, 3, 0),
(3, 'Dragonfly', 3, 'Abdominals', 1, 4, 3, 0),
(4, 'Ring dips', 1, 'Triceps', 2, 4, 3, 0),
(5, 'Ring dip hold', 1, 'Triceps', 3, 3, 3, 0),
(6, 'Atg squats', 4, 'Quads', 3, 3, 3, 0),
(7, 'Pistol squats', 4, 'Quads', 1, 4, 3, 0),
(8, 'Chair hold', 4, 'Quads', 3, 1, 3, 0),
(9, 'Calf raises', 4, 'Calf', 3, 1, 3, 0),
(10, 'Iso super-sumos', 4, 'Quads', 3, 1, 3, 0),
(11, 'Forward-lunge, Knee', 4, 'Glutes', 2, 2, 3, 0),
(12, 'Low-range Squat pump', 4, 'Quads', 3, 2, 3, 0),
(13, 'Power Isometric Shoulder Press', 1, 'Shoulders', 2, 1, 3, 0),
(14, 'Isometric Lateral Raise', 1, 'Shoulders', 2, 1, 3, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
