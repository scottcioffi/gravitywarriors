-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jul 16, 2014 at 10:42 PM
-- Server version: 5.5.34
-- PHP Version: 5.5.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `gw`
--

-- --------------------------------------------------------

--
-- Table structure for table `exercises`
--

CREATE TABLE `exercises` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `target` tinyint(4) NOT NULL,
  `primarygroup` enum('Abdominals','Biceps','Shoulders','Low Back','Calf','Glutes','Hamstrings','Back','Obliques','Chest','Quads','Traps','Triceps') NOT NULL,
  `volume` tinyint(4) NOT NULL,
  `startingrank` tinyint(4) NOT NULL,
  `favorite` tinyint(4) NOT NULL DEFAULT '3',
  `ban` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `exercises`
--

INSERT INTO `exercises` (`id`, `name`, `target`, `primarygroup`, `volume`, `startingrank`, `favorite`, `ban`) VALUES
(1, 'Pushups', 1, 'Chest', 3, 2, 3, 0),
(2, 'Pullup', 2, 'Back', 2, 3, 3, 0),
(3, 'Dragonfly', 3, 'Abdominals', 1, 4, 3, 0),
(4, 'Ring dips', 1, 'Triceps', 2, 4, 3, 0),
(5, 'Ring dip hold', 1, 'Triceps', 3, 3, 3, 0),
(6, 'Atg squats', 4, 'Quads', 3, 3, 3, 0),
(7, 'Pistol squats', 4, 'Quads', 1, 4, 3, 0),
(8, 'Chair hold', 4, 'Quads', 3, 1, 3, 0),
(9, 'Calf raises', 4, 'Calf', 3, 1, 3, 0);
