-- MySQL dump 10.13  Distrib 5.5.42, for osx10.6 (i386)
--
-- Host: localhost    Database: gw
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `exercises`
--

DROP TABLE IF EXISTS `exercises`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exercises` (
  `id` smallint(6) NOT NULL AUTO_INCREMENT,
  `name` text NOT NULL,
  `target` tinyint(4) NOT NULL,
  `primarygroup` enum('Abdominals','Biceps','Shoulders','Low Back','Calf','Glutes','Hamstrings','Back','Obliques','Chest','Quads','Traps','Triceps') NOT NULL,
  `volume` tinyint(4) NOT NULL,
  `startingrank` tinyint(4) NOT NULL,
  `favorite` tinyint(4) NOT NULL DEFAULT '3',
  `ban` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exercises`
--

LOCK TABLES `exercises` WRITE;
/*!40000 ALTER TABLE `exercises` DISABLE KEYS */;
INSERT INTO `exercises` VALUES (1,'Pushups',1,'Chest',3,2,3,0),(2,'Pullup',2,'Back',2,3,3,0),(3,'Dragonfly',3,'Abdominals',1,4,3,0),(4,'Ring dips',1,'Triceps',2,4,3,0),(5,'Ring dip hold',1,'Triceps',3,3,3,0),(6,'Atg squats',4,'Quads',3,3,3,0),(7,'Pistol squats',4,'Quads',1,4,3,0),(8,'Chair hold',4,'Quads',3,1,3,0),(9,'Calf raises',4,'Calf',3,1,3,0);
/*!40000 ALTER TABLE `exercises` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workout1`
--

DROP TABLE IF EXISTS `workout1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `workout1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `exerciseid` int(11) NOT NULL,
  `workoutid` int(11) NOT NULL,
  `position` tinyint(4) NOT NULL,
  `totalreps` smallint(6) NOT NULL,
  `1` smallint(6) NOT NULL,
  `2` smallint(6) NOT NULL,
  `3` smallint(6) NOT NULL,
  `4` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workout1`
--

LOCK TABLES `workout1` WRITE;
/*!40000 ALTER TABLE `workout1` DISABLE KEYS */;
INSERT INTO `workout1` VALUES (1,6,1,1,0,0,0,0,0),(2,8,1,2,2000,24,240,20,2000);
/*!40000 ALTER TABLE `workout1` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-06  9:26:31
