<?php
include ('connect.php');

if(isset($_POST['action'])){
	$action=$_POST['action'];	
}
else{
	$action="";
}
/************************************************** ACTION FUNCTIONS **************************************************/
if($action=='runquery'){
	$connection = mysqli_connect($server, $user_name, $password, $database);
	$sql = $_POST['sql'];
	$sql2 = $_POST['sql2'];
	$sql3 = $_POST['sql3'];
	$query=$sql.$sql2.$sql3;
	echo $query;
	$result=mysqli_multi_query($connection,$query);
	if (!$result){
  		die(mysql_error());
  	}
  	else{
  		mysqli_close($connection);
  	}
}

if($action=='getidofnextsavedworkout'){
	$connection = mysql_connect($server, $user_name, $password);
	$connected = mysql_select_db($database, $connection);
	$SQL = "SELECT MAX(id) as nextid FROM savedworkouts";
	$result = mysql_query($SQL);
	$e = mysql_fetch_assoc($result);
	echo($e['nextid']+1);
}

if($action=='showexercises'){
	$mysqli = new mysqli($server, $user_name, $password);

	// Check connection
	if ($mysqli -> connect_errno) {
	  echo "Failed to connect to MySQL: " . $mysqli -> connect_error;
	  exit();
	}

	//$connection = mysql_connect($server, $user_name, $password);
	$mysqli -> select_db($database);
	//$result = mysql_query($SQL);
	//$connected = mysql_select_db($database, $connection);
	$SQL = "SELECT * FROM exercises WHERE ban=0";
	$result= $mysqli -> query($SQL);
	
	echo '
	
	<table>
		<tr>
			<th></th>
			<th>Exercise Name</th>
			<th>Target Area</th>
			<th>Primary Muscle Group</th>
			<th>Volume Level</th>
			<th>Minimum Rank</th>
			<th>Favoritism</th>
		</tr>
	';
	//$row = $result -> fetch_assoc();

	//$result = mysqli_query($mysqli, $query);

/* fetch associative array */
while ($d = mysqli_fetch_assoc($result)) {
 /*   printf("%s (%s)\n", $row["Name"], $row["CountryCode"]);
}


	while ( $d = mysql_fetch_assoc($result) ) {*/
		echo'
		<tr id="exercise'.$d['id'].'">
			<td><a title="Edit" onclick="editexercise(\''.$d['id'].'\')"><i class="fa fa-pencil-square-o"></i></a><a title="Add Exercise to Routine" onclick="$e=$(this).closest(\'tr\'); addexercise(\''.$d['id'].'\', \''.$d['name'].'\', $e)"><i class="fa fa-plus-circle"></i></a></td>
			<td>'.$d['name'].'</td>
			<td>'.targeted($d['target']).'</td>
			<td>'.$d['primarygroup'].'</td>
			<td title="Rough Estimate of rep amount expected at blackbelt level...

1:  1 - 10 reps  (OR <=10 seconds).  &#013;2: 11 - 20 reps (OR 11-30 seconds) &#013;3: 20 - 100+ reps (OR > 30 seconds)."> '.$d['volume'].'</td>
			<td >'.rank($d['startingrank']).'</td>
			<td title="(1 - 5 Scale)

			Probability of selection via the random generator
			Higher numbers yield higher probability.">'.$d['favorite'].'</td>
		</tr>';
	}
	echo '
	</table>';
}
/************************************************** OTHER FUNCTIONS **************************************************/
function targeted($t){
	switch($t){
		case 1:
			$target="Upper body push";
			break;
		case 2:
			$target="Upper body pull";
			break;
		case 3:
			$target="Core";
			break;
		case 4: 
			$target="Lower body";
			break;
	}
	return $target;
}
function rank($r){
	switch($r){
		case 0:
			$rank="White Belt";
			break;
		case 1:
			$rank="Blue Belt";
			break;
		case 2:
			$rank="Purple Belt";
			break;
		case 3: 

			$rank="Brown Belt";
			break;
		case 4: 
			$rank="Black Belt";
			break;
	}
	return $rank;
}
?>