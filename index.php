<!DOCTYPE>
<html>
	<head>
		<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
		<!--<script src="js/jquery-ui.min.js"></script>
	<link href="css/jquery-ui.css" type="text/css" rel="stylesheet">
		<link href="css/jquery-ui.structure.css" type="text/css" rel="stylesheet">
		<link href="css/jquery-ui.theme.css" type="text/css" rel="stylesheet">

		-->		
		<?php
		error_reporting(-1);
		ini_set('display_errors', 'On');
 		include('connect.php');
		//require ("auth/auth.class.php");
		//$app = new auth();
		//$app->register("elena", "pocahontas", "pocahontas", "elenagmz21@gmail.com");
		 //$password, $verifypassword, $email)

		 ?>
		<script src="js/jquery.js"></script>
		<script src="js/jquery-ui.js"></script>
		<link href="css/style.css" type="text/css" rel="stylesheet">
		<link href="css/jquery-ui.css" type="text/css" rel="stylesheet">
		<link href="css/jquery-ui.structure.css" type="text/css" rel="stylesheet">
		<link href="css/jquery-ui.theme.css" type="text/css" rel="stylesheet">
		<link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet">
	</head>
	<body>
	<div id="tabs">
		<ul>
			<li><a href="#exercises">Exercise Archive</a></li>
			<li><a href="#routine">routine</a></li>
		</ul>
		<div id="exercises">
			<h1>Exercise Archive</h1>
			<div id="exerciselist">
			</div>
			<div id="excercisefilter" class="footer">
				<h1>Filter</h1>
			</div>
		</div>
		<div id="routine">
			<span class="disabled">save</span> <span>load</span>
			<!--<div class="head">
				TEST
			</div>-->
			<h1>You have no exercises in this workout.</h1>
			<ul>
			</ul>
			<div id="loaded">
				<div>
					<table>
						<tr>
							<td>Workout 1</td>
							<td>2014-02-22</td>
							<td>Delete</td>
						</tr>
						<tr>
							<td>Workout 1</td>
							<td>2014-02-22</td>
							<td>Delete</td>
						</tr>
						<tr>
							<td>Workout 1</td>
							<td>2014-02-22</td>
							<td>Delete</td>
						</tr>
						<tr>
							<td>Workout 1</td>
							<td>2014-02-22</td>
							<td>Delete</td>
						</tr>			
					</table>
				</div>
			</div>
		</div>
	</div>
	<div id="alerts">
		<div id="dialog-message" title="NO EXERCISES">
			You have no exercises in this workout.  Add exercises before saving.
		</div>
		<div id="dialog-form" title="SAVE WORKOUT">
		  <p class="validateTips">You must enter a name.</p>
		  <form>
		    <fieldset>
		      <label for="name">Workout Name</label>
		      <input type="text" name="name" id="name" class="text ui-widget-content ui-corner-all">
		      <!-- Allow form submission with keyboard without duplicating the dialog button -->
		      <input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
		    </fieldset>
		  </form>
		</div>
	</div>
		<script src="js/fx.js"></script>
	</body>
</html>