//fx.js
$(document).ready(function() {
	/**** FUNCTIONS ****/
	
	/**** desired operations for tomorro:
	show number of totalreps | set1reps | set2reps | set3reps| set4reps | total 
		total is (red until complete, then green and stop at 0)
	generate the order number

	/**** STATIC OPERATIONS ****/


	/**** LISTENERS****/
	$('#routine span:first-of-type').click(function(){
		if( !($(this).hasClass('disabled')) ){
			
			saveroutine();
		}
		else{
			//alert('0');
			$(function(){
				$("#dialog-message").dialog({
					modal: true,
					buttons: {
						Ok: function(){
							$(this).dialog("close");
						}
					}
				});
			});
			//$(".ui-dialog-titlebar").css('background-color', 'white', 'color', 'black');
			$(".ui-dialog-titlebar").focus();
		}
	});
	$('#routine span:nth-of-type(2)').click(function(){
		$("#loaded").css('visibility', 'visible');
	});
	$('#exerciselist').disableSelection();	
	$('#routine').disableSelection();	
	$('#routine input[type=text]').enableSelection();	
	$("body").tooltip({
		fadeInSpeed: 1000, 
		opacity: .2
		});
	$('#tabs').tabs({
		activate: propogateexercises()
	});
	$('#routine ul').sortable({
  	start : function(event, ui){ 
   		ui.item.css({
		    //"background-color" : "638296";
			})
    },
    stop : function(event, ui){ 
      ui.item.css('background-color', '#444');
  		if (Math.abs(ui.offset.left - ui.originalPosition.left)>300){
  			ui.item.remove();
  			if( $("#routine ul li").length == 0 ){
  				$("#routine>h1").show();
  				$("#routine span:first-of-type").addClass("disabled");
  			}
  		}
    },
  	out: function( event, ui ){
  		if (Math.abs(ui.offset.left - ui.originalPosition.left)>250){
  			ui.item.css('background-color', 'red');
  		}
  		if (Math.abs(ui.offset.left - ui.originalPosition.left)<250){
  			ui.item.css('background-color', '444');
  		}
  	}
	});
	//$(".titlebar").disableSelection().css('webkit-user-select','none');
});


$( document ).ajaxStop( function() {
	$('#exerciselist').disableSelection();	
	$("#exerciselist table td").tooltip({
		position: {
	    my: "center bottom",
	    at: "right top"
	  },
		show: { effect: "blind", duration: 0 },
		fadeInSpeed: 1000, 
		opacity: 0.2
		});
});
/**** FUNCTIONS ****/

function sendid(x){
	return x;
}

function getnextworkoutid(){		
	var id;
	$.ajax({
		type: "POST",
		url: 'fx.php',
		data: "&action=getidofnextsavedworkout", // serializes the form's elements.
		async: false,
		success: function(data)
		{
		  id=data;
		}
	});
	return id;
}


//function saveroutine(){
function saveroutine(wid, wname){
	if ( (!(wid==undefined)) && (!(wname==undefined)) ){
		id=wid;
		name=wname;
	}else{
		id=getnextworkoutid();
		name="CURRENT_DATE()";
	}
	addexercises="";
	//query the database to get the last id
	
	/*
	build a sql statement to :
		1) create a table called workoutX and add the various exercises as values, reading from the ul. 
		2) add an entry to the updatesavedworkouts table.
	*/ 
	var numitems = $("#routine ul li").length;
	var position;
	var index;
	for (position = 1; position < numitems+1; ++position) {
		totalreps=$("#routine li:nth-of-type("+position+") input[type='text']:nth-of-type(1)").val();
		set1=$("#routine li:nth-of-type("+position+") tr:nth-of-type(2) td:nth-of-type(2) input[type='text']").val();
		set2=$("#routine li:nth-of-type("+position+") tr:nth-of-type(2) td:nth-of-type(3) input[type='text']").val();
		set3=$("#routine li:nth-of-type("+position+") tr:nth-of-type(2) td:nth-of-type(4) input[type='text']").val();
		set4=$("#routine li:nth-of-type("+position+") tr:nth-of-type(2) td:nth-of-type(5) input[type='text']").val();

		exerciseid=$("#routine li:nth-of-type("+position+") input[type='hidden']").val();
		//id=10;
		//addexercises+="INSERT INTO `gw`.`workouts` (`id`, `uid`, `exerciseid`, `workoutid`, `position`, `totalreps`, `1`, `2`, `3`, `4`) VALUES (NULL, '"+addid+"', '"+id+"', `0`, '"+position+"', '"+totalreps+"', '"+set1+"', '"+set2+"', '"+set3+"', '"+set4+"');";	   
		addexercises+="INSERT INTO `gw`.`sets` 	(`id`, `uid`, `exerciseid`, `workoutid`, `position`, `totalreps`, `1`, `2`, `3`, `4`) VALUES (NULL, '0', '"+exerciseid+"', '01', '"+position+"', '"+totalreps+"', '"+set1+"', '"+set2+"', '"+set3+"', '"+set4+"');";	   
	}

	if(runquery(addexercises)){
		alert ('added!');
	}
	updateUX($("#routine li"));
	$("#routine>h1").show();
	$("#routine>h1").html('name');
/*
		addid=$("#routine li:nth-of-type("+position+") input[type='hidden']").val();
		//id=10;
		addexercises+="INSERT INTO `gw`.`workout"+id+"` (`id`, `exerciseid`, `workoutid`, `position`, `totalreps`, `1`, `2`, `3`, `4`) VALUES (NULL, '"+addid+"', '"+id+"', '"+position+"', '"+totalreps+"', '"+set1+"', '"+set2+"', '"+set3+"', '"+set4+"');";	   
	}
	addworkout="INSERT INTO `gw`.`savedworkouts` (`id`, `userid`, `date`, `name`) VALUES ('"+id+"', '0', CURRENT_DATE(), '"+name+"');";
	
	//populate the table
	//foreach li in ul, build this statement:
//INSERT INTO `gw`.`workout1` (`id`, `exerciseid`, `workoutid`, `position`, `1`, `2`, `3`, `4`) VALUES (NULL, '2', '4', '1', '10', '9', '8', '7');
//append it to the addexercises variable.
	//
	var sql= "CREATE TABLE `workout"+id+"` (`id` int(11) NOT NULL AUTO_INCREMENT,`exerciseid` int(11) NOT NULL,`workoutid` int(11) NOT NULL,`position` tinyint(4) NOT NULL,`totalreps` smallint(6) NOT NULL,`1` smallint(6) NOT NULL,`2` smallint(6) NOT NULL,`3` smallint(6) NOT NULL,`4` smallint(6) NOT NULL,PRIMARY KEY (`id`)) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;";//+addexercises;//+addworkout;
	var sql2=addexercises;
	var sql3=addworkout;
	var udpatesavedworkouts= "";
	if(inserttotable(sql,sql2,sql3)){
		alert ('added!');
	}
	updateUX($("#routine li"));
*/
	// now we'll need to santise on the php; but otherwise allow the insert, and give back a success call
	// on success, indicate in a smooth on-screen alert.

}

function runquery(sql, sql2, sql3){

//function inserttotable(sql, sql2, sql3){
	var success;
	$.ajax({
		type: "POST",
		url: 'fx.php',
		data: "&action=runquery&sql="+sql+"&sql2="+sql2+"&sql3="+sql3, // serializes the form's elements.
//		data: "&action=inserttotable&sql="+sql+"&sql2="+sql2+"&sql3="+sql3, // serializes the form's elements.
		async: false,
		success: function(data)
		{
		  //alert(data+'...');
		}
	});
	return success;
}


function addexercise(id, name, el){
	add='\
	<li>\
	<h1><img src="images/v3.png" />'+name+'</h1><input type="hidden" value="'+id+'" />\
		<div>\
			<table>\
				<tr>\
					<th>Total Reps</th>\
					<th>Set 1 reps</th>\
					<th>Set 2 reps</th>\
					<th>Set 3 reps</th>\
					<th>Set 4 reps</th>\
					<th>Remaining</th>\
				</tr>\
				<tr>\
				<td>\
					<input type="text" title="Total Reps"/></th>\
					<td><input type="text" title="Set 1 Reps"></th>\
					<td><input type="text" title="Set 2 Reps"></th>\
					<td><input type="text" title="Set 3 Reps"></th>\
					<td><input type="text" title="Set 4 Reps"></th>\
					<td><span title="Remaining Reps"></span></th>\
				</tr>\
		</div>\
	</li>';
	$('#routine ul').append(add);
	$('#routine td input[type="text"]').bind("change paste keyup", function() {
		calculateremainingreps($(this));
	});
	$("#routine>h1").hide();
	$("#routine span:first-of-type").removeClass("disabled");
	updateUX(el.closest("tr"));
}

function updateUX(e){
	e.effect("highlight", { times:1, color: "#fcc319"},1000);
	//e.effect("highlight", { times:1, color: "#fcc319"},200);
	/*color=e.css('background-color');
 	e.animate( { backgroundColor: "#f00" }, 2000 )
  e.animate( { backgroundColor: color}, 2000 );*/
}

function propogateexercises(){		
	$.ajax({
		type: "POST",
		url: 'fx.php',
		data: "&action=showexercises", // serializes the form's elements.
		success: function(data)
		{
	   $("#exerciselist").html(data); // show response from the php script.
	   $("#exerciselist td").dblclick(function(data){
	   	$line=$(this).closest('tr');
	   	exerciseid=($line.attr('id')).substring(8);
	   	exercisename=$line.children("td:nth-of-type(2)").html();
	   	addexercise(exerciseid, exercisename,$(this));
	   });
		}
	});
}	

function calculateremainingreps(e){
	//e.css('background-color', 'yellow');
	text=e.val();
	pattern=/[^\d]/;
	if((pattern.test(text))){
		e.val(e.val().replace(/[^\d]/,''));
	}
	$mothertr=e.parents('tr');
	$total=$mothertr.children('td:first-of-type').children('input[type=text]');
	$set1=$mothertr.children('td:nth-of-type(2)').children('input[type=text]');
	$set2=$mothertr.children('td:nth-of-type(3)').children('input[type=text]');
	$set3=$mothertr.children('td:nth-of-type(4)').children('input[type=text]');
	$set4=$mothertr.children('td:nth-of-type(5)').children('input[type=text]');
	$remaining=$mothertr.children('td:nth-of-type(6)').children('span');
	remaining=Math.max(0,($total.val()-$set1.val()-$set2.val()-$set3.val()-$set4.val()));
	$remaining.html(remaining);
	if ((remaining==0) && ($total.val()!='')){
		$mothertr.children('td').children('input[type=text], span').addClass('done');
	}
	else{
		$mothertr.children('td').children('input[type=text], span').removeClass('done');
	}
}
